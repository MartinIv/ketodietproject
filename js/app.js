function slider() {
	const elem = document.getElementById("first-article");
	if (elem.style.display === "none") {
	  elem.style.display = "block";
	} else {
	  elem.style.display = "none";
	}

	const elem2 = document.getElementById("second-article");
	// elem2.style.display = "block";
	if (elem2.style.display === "none") {
	  elem2.style.display = "block";
	} else {
	  elem2.style.display = "none";
	}
  }
  const menuExpander = () => {
	const menu = document.getElementById("menu-expander");

	menu.style.display = menu.style.display === "flex" ? "none" : "flex";
  };

  function menu()
  {
	const nav = document.querySelector('header > nav');
	const btn = document.querySelector('header > button.menu');

	btn.addEventListener('click', function()
	{
		if (nav.className === 'open')
		{
			nav.className = '';
			btn.className = 'menu';
		}
		else
		{
			nav.className = 'open';
			btn.className = 'menu open';
		}
	});
  }

  menu();

  function toggleTheme()
  {
	const body = document.querySelector('body');
	const toggle = document.getElementById('toggle-theme');

	toggle.addEventListener('change', function()
	{
		if (toggle.checked)
		{
			body.className = 'dark';
		}
		else
		{
			body.className = '';
		}
	});
  }

  toggleTheme();